package com.social.dao;

import com.social.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long> {
    public Persona findOneByCedula(String cedula);
    public List<Persona> findAll();

}