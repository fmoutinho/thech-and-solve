package com.social.dao;

import com.social.entities.Data;
import com.social.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  DataRepository extends JpaRepository<Data, Long> {
    public Data findOneByCedula(String cedula);
}