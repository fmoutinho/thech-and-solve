package com.social.entities;

import org.springframework.context.annotation.Scope;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
@Entity
@Table(name="Data")
@Scope("session")
public class Data implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id ;

    private int numeroDiasTrabajo;
    private int cantidadElementos;

    private int elementoSuperior;
    private int numeroViajesMaximosPorDia;
    private String cedula;

    public Data() {
    }

    public Data(int numeroDiasTrabajo, int cantidadElementos, int elementoSuperior, int numeroViajesMaximosPorDia, String cedula) {
        this.numeroDiasTrabajo = numeroDiasTrabajo;
        this.cantidadElementos = cantidadElementos;
        this.elementoSuperior = elementoSuperior;
        this.numeroViajesMaximosPorDia = numeroViajesMaximosPorDia;
        this.cedula = cedula;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumeroDiasTrabajo() {
        return numeroDiasTrabajo;
    }

    public void setNumeroDiasTrabajo(int numeroDiasTrabajo) {
        this.numeroDiasTrabajo = numeroDiasTrabajo;
    }

    public int getCantidadElementos() {
        return cantidadElementos;
    }

    public void setCantidadElementos(int cantidadElementos) {
        this.cantidadElementos = cantidadElementos;
    }



    public int getElementoSuperior() {
        return elementoSuperior;
    }

    public void setElementoSuperior(int elementoSuperior) {
        this.elementoSuperior = elementoSuperior;
    }

    public int getNumeroViajesMaximosPorDia() {
        return numeroViajesMaximosPorDia;
    }

    public void setNumeroViajesMaximosPorDia(int numeroViajesMaximosPorDia) {
        this.numeroViajesMaximosPorDia = numeroViajesMaximosPorDia;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
}