package com.social.controller;

import java.security.Principal;

import com.social.dao.DataRepository;
import com.social.dao.PersonaRepository;
import com.social.entities.Data;
import com.social.entities.Persona;
import com.social.services.FileService;
import com.social.services.PersonaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.social.services.UserService;
import com.social.util.CustomErrorType;
import com.social.entities.User;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Fernando Moutinho
 *
 */
@RestController
@RequestMapping("account")
public class AccountController {

	public static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private UserService userService;

	@Autowired
	PersonaService personaService;
	@Autowired
	DataRepository dataRepository;

	// request method to create a new account by a guest
	@CrossOrigin
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody User newUser) {
		if (userService.find(newUser.getUsername()) != null) {
			logger.error("Este usuario ya existe " + newUser.getUsername());
			return new ResponseEntity(
					new CustomErrorType("El Usuario : " + newUser.getUsername() + "Ya se encuentra registrado en Base de datos "),
					HttpStatus.CONFLICT);
		}
		newUser.setRole("ADMINISTRADOR");
		newUser.setFullName(newUser.getFullName());
		
		return new ResponseEntity<User>(userService.save(newUser), HttpStatus.CREATED);
	}



	@CrossOrigin
	@RequestMapping(value = "profile/actividades", method = RequestMethod.POST)
	public ResponseEntity<?> createActividad(@RequestBody Persona persona) {
		if (personaService.find(persona.getCedula()) != null) {
			logger.error("Persona ya existe " + persona.getNombres());
			return new ResponseEntity(
					new CustomErrorType("Persona ya existe " + persona.getNombres() + "ya esta en Bases de datos "),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Persona>(personaService.save(persona), HttpStatus.CREATED);
	}



	// this is the login api/service
	@CrossOrigin
	@RequestMapping("/login")
	public Principal user(Principal principal) {
		logger.info("user logged "+principal);
		return principal;
	}

	
	
}
