package com.social.controller;

//PAQUETES A IMPORTAR

import com.social.entities.Data;
import com.social.entities.Persona;
import com.social.services.DataService;
import com.social.services.FileService;
import com.social.services.PersonaService;
import com.social.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import sun.misc.Resource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
@RequestMapping("account")
public class FileController {
    private final  int PESOREQUERIDO=50;//Es el peso constante requerido.
    private final String  MENSAJE ="ARCHIVO CARGADO EXITOSAMENTE !!";
    private final String  MENSAJEERROR ="ARCHIVO NO CARGADO EXITOSAMENTE !!";
    List<String> files = new ArrayList<String>();
    @Autowired
    FileService fileservice;
    private final Path rootLocation =   Paths.get("ProfilePictureStore");
    @Autowired
    PersonaService personaService;
    @Autowired
    DataService dataService;
    @Autowired
    StorageService storageService;
    ArrayList lineas = new ArrayList();
    List<String> fileNames = new ArrayList<String>();
    int lineaElemento=0;
    @CrossOrigin(origins = "http://localhost:4200") // Call  from Local Angualar
    @PostMapping("/profile/data")
    public ResponseEntity < String > handleFileUpload(@RequestParam("file") MultipartFile file,@RequestParam("cedula") String cedula) throws IOException {
       lineaElemento=0;
        String message = "";
        String root = System.getProperty("user.dir");
        String filepath = "\\ProfilePictureStore\\"+file.getOriginalFilename(); // in case of Windows: "\\path \\to\\yourfile.txt
        String abspath = root+filepath;
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        int indice = 0;
        try {
            fileservice.store(file);
            message = MENSAJE + file.getOriginalFilename() ;
            fr = new FileReader (abspath);
            br = new BufferedReader(fr);
            // Lectura del fichero
            String linea;
            while((linea=br.readLine())!=null) {
              //CREO UN ARREGLO DE ARRAY LIST
                lineas.add(linea);
                lineaElemento++;

            }
            //INICIALIZO EL ARREGLO DE ENTEROS CON LA CANTIDAD DE ELEMENTOS PROCESADOS EN EL ARCHIVO.
            int ElementosN[] = new int[lineas.size()];
            for (int j = 0 ; j < lineas.size() ; j++){
                linea = (String)lineas.get(j);
                fileNames.add(linea);
                ElementosN[indice] = Integer.valueOf(linea);
                indice++;
            }
            //OBTENEMOS LA CANTIDAD DE ELEMENTOS
            //LA IDEA ES UNA VES CARAGADO EL ARCHIVO Y LUEGO DE HACE EL PROCESO BORRAR EL ARCHIVO
            Persona persona =  personaService.find(cedula.trim());
            Data data = new Data();
            data.setCedula(persona.getCedula());
            data.setNumeroDiasTrabajo(Integer.valueOf(persona.getDiasTrabajados()));
            data.setCantidadElementos(lineaElemento);
            data.setElementoSuperior(5);
            data.setNumeroViajesMaximosPorDia(5);
            dataService.save(data);
           processoActividades(data.getNumeroDiasTrabajo(),ElementosN);
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = MENSAJEERROR + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }
    @CrossOrigin(maxAge = 3600)
    @GetMapping(path = "/actividades/", produces = "application/json;charset=UTF-8")
    public Persona getPersonaProceso(@RequestParam(value = "cedula", required = false) String cedula ) {
        try {
            Persona persona ;
            persona = personaService.find(cedula.toString().trim());
            Data data = new Data();
            data.setCedula(persona.getCedula());
            data.setNumeroDiasTrabajo(Integer.valueOf(persona.getDiasTrabajados()));
            data.setCantidadElementos(lineaElemento);
            data.setElementoSuperior(5);
            data.setNumeroViajesMaximosPorDia(5);
            dataService.save(data);
            return personaService.find(cedula.trim()) ;
        }catch (Exception e){
            System.out.println(e);
        }
            return personaService.find(cedula);
    }

    @GetMapping("/getallfiles")
    public ResponseEntity<List<String>> getListFiles(Model model) {
        fileNames
                .stream().map(fileName -> MvcUriComponentsBuilder
                        .fromMethodName(FileController.class, "getFile", fileName).build().toString())
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(fileNames);
    }
    @GetMapping(path = "/personas", produces = "application/json;charset=UTF-8")
    public List<Persona> getPersonas(Persona persona) {
        return personaService.findAll();
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = (Resource) storageService.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName()+ "\"")
                .body(file);
    }


    //FUNCION PARA CONVERTIR STRING A ENTERO CON FINALIDAD DE FORMAR UN ARREGLO DE ENTEROS
    public int[] parse(String num){
        int[] nums = new int[num.length()];
        for(int i=0; i<num.length(); i++){
            nums[i] = Character.getNumericValue(num.charAt(i));
            System.out.println("Elementos en la Posicion :"+i);
            System.out.println("PESO:"+ nums[i]);
        }
        return nums;
    }

    public static void archivos(String directorioRaiz) {
        File carpeta = new File(directorioRaiz);
        if (carpeta.exists()) {
            File[] ficheros = carpeta.listFiles(); //Listar archivos en carpeta raiz
            for (File f : ficheros) {
                System.out.println("Archivo Obtenido en la funcion Archivos :"+f.getName());


            }
        } else {

        }
    }
    public void processoActividades(int diaTrabajados,int ElementosN[]){
        //EVALUANDO QUE PARA EL DIA DE HOY HAY N ELEMENTOS PARA SER MOVIDOS POR WILSON
        int NumeroMaximoViajes=0;
        int sumaPesoElemento=0;
        int numeroElementos=0;
        int PesoTotal=0;
        numeroElementos =ElementosN.length;
        PesoTotal = ElementosN.length*ElementosN[ElementosN.length-1];//PESO DE LA BOLSA K*W , donde K cantidad de elementos y W , peso del Elemento superior
        for(int i=0;i<diaTrabajados;i++) {
            for (int j = 0; j < ElementosN.length; j++) {
                switch (i){
                    case 1:
                        if(diaTrabajados<=500){
                            if(numeroElementos<=100){
                                if(ElementosN[j]<=100){
                                    sumaPesoElemento=sumaPesoElemento+ ElementosN[j];
                                    NumeroMaximoViajes++;
                                }
                            }
                        }
                        break;
                    case 2:
                        if(diaTrabajados<=500){
                            if(numeroElementos<=100){
                                if(ElementosN[j]<=100){
                                    sumaPesoElemento=sumaPesoElemento+ ElementosN[j];
                                    NumeroMaximoViajes++;
                                }
                            }
                        }
                        break;
                    case 3:if(diaTrabajados<=500){
                        if(numeroElementos<=100){
                            if(ElementosN[j]<=100){
                                sumaPesoElemento=sumaPesoElemento+ ElementosN[j];
                                NumeroMaximoViajes++;
                            }
                        }
                    }
                        break;
                    default:
                        if(diaTrabajados<=500){
                        if(numeroElementos<=100){
                            if(ElementosN[j]<=100){
                                sumaPesoElemento=sumaPesoElemento+ ElementosN[j];
                                NumeroMaximoViajes++;
                            }
                        }
                    }
                        break;
                }

            }
            if(sumaPesoElemento>=PESOREQUERIDO){

                System.out.println("Case #"+i+":"+NumeroMaximoViajes);
            }
        }
        System.out.println("Peso total de la bolsa:"+PesoTotal);
        System.out.println("Numero de Viajes Maximos:"+NumeroMaximoViajes);

    }
}