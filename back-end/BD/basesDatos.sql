-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: spring_auth
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad_elementos` int(11) NOT NULL,
  `cedula` varchar(255) DEFAULT NULL,
  `elemento_superior` int(11) NOT NULL,
  `numero_dias_trabajo` int(11) NOT NULL,
  `numero_viajes_maximos_por_dia` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data`
--

LOCK TABLES `data` WRITE;
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
INSERT INTO `data` VALUES (1,40,'19614680',5,5,5),(2,80,'19955988',5,5,5),(3,120,'19955988',5,5,5),(4,40,'181818',5,5,5),(5,40,'456783',5,5,5),(6,40,'123333',5,5,5),(7,40,'55555',5,5,5),(8,40,'66556',5,2,5),(9,10,'555544',5,5,5),(10,40,'199999999',5,5,5),(11,40,'543314',5,5,5),(12,50,'543314',5,5,5),(13,10,'858585',5,5,5),(14,10,'3456783131',5,5,5),(15,40,'1414141',5,5,5),(16,10,'22220200202',5,5,5),(17,40,'9999999',5,5,5),(18,10,'1988888856',5,5,5),(19,40,'8787878',5,5,5),(20,40,'0099999',5,5,5),(21,10,'7777676',5,5,5),(22,40,'98989896',5,5,5),(23,40,'23456780000',5,5,5),(24,40,'19988966',5,5,5),(25,2,'555214131',5,5,5),(26,40,'5432345432345',5,5,5),(27,40,'656565655',5,5,5),(28,40,'151515',5,5,5),(29,40,'555551131',5,5,5),(30,40,'5432345654345676543',5,5,5),(31,2,'654345676',5,5,5),(32,40,'551513131',5,5,5),(33,40,'19678680',5,5,5);
/*!40000 ALTER TABLE `data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apellidos` varchar(255) DEFAULT NULL,
  `cedula` varchar(255) DEFAULT NULL,
  `nombres` varchar(255) DEFAULT NULL,
  `dias_trabajados` int(11) DEFAULT NULL,
  `horas_por_dia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_cm8asjmty9arx6nqo43mfl0i6` (`cedula`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (84,'543','543','kjhgfd',5,5),(85,'gsgsgsg','54323','gfd',5,5),(86,'gsgs','424242','ytre',5,5),(87,'dadada','6543','jhgfd',5,5),(88,'Perez Gonzalez','19988966','Pedro Peito',5,5),(89,'hghgf','45678','hgfd',5,5),(90,'mmgmgmg','50505050','prueba',5,5),(91,'jhgf','55555','jhgre',5,5),(92,'hgfd','5555533','hgfd',5,5),(93,'jhgfd','55555333','kjhgf',55,55),(94,'jhgf','88888','hgfd',5,5),(95,'fafa','66655','fsfsfs',5,5),(96,'dacdada','45434141','gfds',5,5),(97,'nbvcx','8765432345678','yhtres',5,5),(98,'faafa','555214131','fafafaf',5,5),(99,'jhgfghjk','678909876543','jhgfd',5,5),(100,'ghgf','56789876543','uytre',5,5),(101,'fafafa','5211','hgfds',5,5),(102,'jhgfdfghj','67543345678','jhgfd',5,5),(103,'hgfd','513131341','jjhgfd',5,5),(104,'lkjhgfd','515151','hgfds',5,5),(105,'hgfdsdfghgfd','5432345432345','jhgtfds',5,5),(106,'gfd','656565655','hgfd',5,5),(107,'jhgfd','151515','hgfd',5,5),(108,'jhgfd','555551131','jhgfd',5,5),(109,'hgfdsdfghgf','5432345654345676543','jhgfd',5,5),(110,'mmmm','654345676','jhgfd',5,5),(111,'kjhgfd','76543','kjhgf',5,5),(112,'hgfdew','551513131','kjhgf',5,5),(113,'perez','19678680','Julie',5,5);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'user.user','1234','administrador','kamalbberriga'),(3,'fernandomoutinho2018@gmail.com','123456','USER','fernando javier'),(4,'moutinho132@gmail.com','1234','ADMINISTRADOR','javier moutinho'),(5,'pati@gmail.com','1234','ADMINISTRADOR','PRATICIA FARO'),(6,'yenni@gmail.com','1234','ADMINISTRADOR','yenni'),(7,'mybeby@hotmail.com','19955988','ADMINISTRADOR','egleydys '),(8,'julie@gmail.com','1234','ADMINISTRADOR','Julie');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-11 20:09:49
