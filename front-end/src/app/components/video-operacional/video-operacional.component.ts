import { Component, OnInit, ViewEncapsulation, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { EmbedVideoService } from 'ngx-embed-video';
import {YoutubeService} from '../../services/youtube.service';
@Component({
  selector: 'app-video-operacional',
  templateUrl: './video-operacional.component.html',
  styleUrls: ['./video-operacional.component.css'],
  encapsulation: ViewEncapsulation.None
})
@Pipe({name: 'safe'})
export class VideoOperacionalComponent implements PipeTransform {
  
  transform(value: any, url: any): any {
    if (value && !url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        let match = value.match(regExp);
        if (match && match[2].length == 11) {
            console.log(match[2]);
            let sepratedID = match[2];
            let embedUrl = '//www.youtube.com/embed/' + sepratedID;
            return this.sanitizer.bypassSecurityTrustResourceUrl(embedUrl);
        }

     }

   }
  youtubeUrl = "https://www.youtube.com/watch?v=6wD4V0rvlDI";
  constructor(private sanitizer: DomSanitizer,private embedService: EmbedVideoService) {
    console.log(this.embedService.embed(this.youtubeUrl));
   }

  ngOnInit() {
  }
  urlSinProcesar = "https://www.youtube.com/watch?v=6wD4V0rvlDI";
  //urlSinProcesar = "//www.youtube.com/embed/8pC5VZM2h8k?rel=0"+1;<--tambien los he visto de esta forma o cualquier entero
  
  urlSaneada = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlSinProcesar);
}
