import { Component, OnInit, Input } from '@angular/core';
 
@Component({
  selector: 'details-upload',
  templateUrl: './details-upload-component.component.html',
  styleUrls: ['./details-upload-component.component.css']
})
export class DetailsUploadComponent implements OnInit {
 
  @Input() fileUpload: string;
 
  constructor() { }
 
  ngOnInit() {
  }
 
}