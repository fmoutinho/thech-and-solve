import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsUploadComponentComponent } from './details-upload-component.component';

describe('DetailsUploadComponentComponent', () => {
  let component: DetailsUploadComponentComponent;
  let fixture: ComponentFixture<DetailsUploadComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsUploadComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsUploadComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
