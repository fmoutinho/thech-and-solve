import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';

import {AppComponent} from "../app.component";
import {Observable} from 'rxjs/Observable';
@Injectable()
export class UploadFileService {
  constructor(private http: HttpClient) {}
  
  pushFileToStorage(file: File,cedula:string): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('cedula',cedula);
    console.log('Archivo a procesar :',file.name);
    const req = new HttpRequest('POST', AppComponent.API_URL+"/account/profile/data", formdata, {
      reportProgress: true,
      responseType: 'text'
    }
    );
    return this.http.request(req);
  }
  getFiles(): Observable<any> {
    return this.http.get(AppComponent.API_URL+'/account/getallfiles');
  }
 
}