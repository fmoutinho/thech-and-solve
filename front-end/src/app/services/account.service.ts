import { Injectable } from '@angular/core';
import {User} from "../model/model.user";
import {Http,Response, Headers,RequestOptions} from "@angular/http";
import { HttpParams, HttpClient ,} from '@angular/common/http';
import {AppComponent} from "../app.component";
import { Persona } from '../model/model.persona';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class AccountService {
  
  constructor(public http: Http) { }

  createAccount(user:User){
    return this.http.post(AppComponent.API_URL+'/account/register',user)
      .map(resp=>resp.json());
  }
  getPersonas(personas:Persona,user:User): Observable<any>{
    let headers = new Headers();
    headers.append('Accept', 'application/json')
    // creating base64 encoded String from user name and password
    var base64Credential: string = btoa( user.username+ ':' + user.password);
    headers.append("Authorization", "Basic " + base64Credential);

    let options = new RequestOptions();
    options.headers=headers;
    let params = new HttpParams();
    params = params.append('cedula', personas.cedula);
  
    const formdata: FormData = new FormData();
    formdata.append('cedula',personas.cedula);
// CONSTRUYE URL GET A WEB SERVICE
    let apiUrl = '/account/actividades/?'+params;
    console.log('Prueba de la cedula que se traera el servicio:'+personas.cedula);
     return this.http.get(AppComponent.API_URL+apiUrl).map(res =>
      res.json()
      );
    //return this.http.get(apiUrl).map(res => res.json());
  }
  getPersona(): Observable<any>{
    return this.http.get(AppComponent.API_URL+'/account/personas');
}

  createPersonaActividad(persona:Persona){
    console.log('Apellidos:',persona.apellidos);
    console.log('Nombres:',persona.nombres);
    console.log('Cedula:',persona.cedula);
    localStorage.setItem('currenPersona', JSON.stringify(persona));
    console.log('Dias Trabajados:',persona.diasTrabajados);
    console.log('Hora Por Dia :',persona.horasPorDia);
    return this.http.post(AppComponent.API_URL+'/account/profile/actividades',persona)
      .map(resp=>resp.json());
     
  }
}
