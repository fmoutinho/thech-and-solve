import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from "./services/auth.service";
import {HttpModule} from "@angular/http";
import { HttpClientModule ,HttpClient} from '@angular/common/http';
import {YoutubeComponent} from './youtube/youtube.component';
import {AccountService} from "./services/account.service";
import {UploadFileService} from "./services/upload-file-service.service";
import { ProfileComponent } from './components/profile/profile.component';
import {routing} from "./app.routing";
import {FacebookModule} from "ngx-facebook";
import {UrlPermission} from "./urlPermission/url.permission";
import { ActividadesComponent } from './components/actividades/actividades.component';
import { VideoOperacionalComponent } from './components/video-operacional/video-operacional.component';
import { EmbedVideo } from 'ngx-embed-video';
import { UrlPipe } from './pipes/url.pipe';
import { ListUploadComponent } from './components/list-upload-component/list-upload-component.component';
import { DetailsUploadComponent } from './components/details-upload-component/details-upload-component.component';
import { PersonaComponent } from './components/persona/persona.component';
import { DataTableModule } from 'angular5-data-table';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    ActividadesComponent,
    YoutubeComponent,ListUploadComponent,
    UrlPipe,
    VideoOperacionalComponent,
    DetailsUploadComponent,
    PersonaComponent,

  ],
  imports: [
    BrowserModule,DataTableModule.forRoot(), EmbedVideo.forRoot(),HttpModule,HttpClientModule,FormsModule,routing, FacebookModule.forRoot(),
  ],
  providers: [AuthService,AccountService,UrlPermission,UploadFileService],
  bootstrap: [AppComponent], schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class AppModule { }
